#! /bin/sh -x

echo "Adding zabbix repo"
sudo rpm -ivh http://repo.zabbix.com/zabbix/3.0/rhel/7/x86_64/zabbix-release-3.0.1.el17.noarch.rpm
sudo yum update -y

echo "Installing zabbix proxy package"
sudo yum install -y zabbix-proxy-mysql