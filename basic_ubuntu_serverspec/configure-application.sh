#!/bin/sh -x

cd /usr/share
sudo git clone https://mircea-colo@bitbucket.org/mircea-colo/microblog.git
cd microblog
sudo python3 -m venv venv
sudo venv/bin/pip3 install -r /tmp/requirements.txt
sudo venv/bin/pip3 install gunicorn