$systemd_unit_file = @(END)
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=ubuntu
Group=root
WorkingDirectory=/usr/share/microblog
ExecStart=/usr/share/microblog/venv/bin/gunicorn -w 2 -b 127.0.0.1:8000 microblog:app

[Install]
WantedBy=multi-user.target
END

lookup('classes', {merge => unique}).include

service { 'gunicorn':
  ensure  => running,
  binary  => '/usr/share/microblog/venv/bin/python3 venv/bin/gunicorn -b localhost:8000 -w 2 microblog:app',
  require => File['/lib/systemd/system/gunicorn.service']
}

file { 'gunicorn.service':
  ensure  => present,
  path    => '/lib/systemd/system/gunicorn.service',
  content => inline_template($systemd_unit_file),
}
